"""
My Flask API, Project 2

Author: Austin Mohler

Date: 10/18/2021
"""
import os
from flask import Flask, abort, render_template #added in render_template from slides

app = Flask(__name__)


@app.route("/")
def hello():
    return "UOCIS docker demo!"

#start of new function I wrote

@app.route("/<path:pathstr>")
def routing(pathstr):
    bad = ("~", "//", "..") # this variable "bad", contatins the parts of the url to look for
    page = os.path.join(os.path.dirname( __file__ ), 'templates')
    list = os.listdir(page)

    if any(ele in pathstr for ele in bad):
        abort(403) #aborting, 403 error
    elif not pathstr in list:
        abort(404) #aborting, 404 error
    elif pathstr in list:
        return render_template(pathstr) #you did it, 200 OK!!



@app.errorhandler(404)
def not_found(e): #function for file_not_found template
    return render_template('404.html') #404 error

@app.errorhandler(403)
def forbidden(e): #function for file_forbidden template
    return render_template('403.html') #403 error


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
